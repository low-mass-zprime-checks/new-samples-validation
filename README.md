# Rivet for low mass Zprime samples validation

## Reference

Newest Rivet tutorial for ATLAS can be found [here](https://gitlab.cern.ch/atlas/athena/-/tree/21.6/Generators/Rivet_i).

## Setup environment

```bash
source setup_env.sh
cd <run folder>
```

## Compile analysis codes

```bash
rivet-build Rivet_Z4mu_Analysis.so Z4mu_Analysis.cc
```

## Setup job options

```bash
python setup_run.py
```

## Run jobs

```bash
cd ZpXXX
athena ZpXXX.py > /dev/null 2>&1 &
```
Since run_0918, an auto_run script is added.