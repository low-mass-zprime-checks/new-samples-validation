import os
import sys

sample_lists = {
    "005": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100000.MGPy8EG_30nlo_Leptophilicmutau_2muZp005_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "007": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100001.MGPy8EG_30nlo_Leptophilicmutau_2muZp007_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "009": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100002.MGPy8EG_30nlo_Leptophilicmutau_2muZp009_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "010": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100090.MGPy8EG_30nlo_Leptophilicmutau_2muZp010_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "011": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100003.MGPy8EG_30nlo_Leptophilicmutau_2muZp011_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "013": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100004.MGPy8EG_30nlo_Leptophilicmutau_2muZp013_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "015": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100005.MGPy8EG_30nlo_Leptophilicmutau_2muZp015_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "017": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100006.MGPy8EG_30nlo_Leptophilicmutau_2muZp017_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "019": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100007.MGPy8EG_30nlo_Leptophilicmutau_2muZp019_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "023": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100008.MGPy8EG_30nlo_Leptophilicmutau_2muZp023_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "027": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100009.MGPy8EG_30nlo_Leptophilicmutau_2muZp027_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "031": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100010.MGPy8EG_30nlo_Leptophilicmutau_2muZp031_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "035": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100011.MGPy8EG_30nlo_Leptophilicmutau_2muZp035_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "039": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100012.MGPy8EG_30nlo_Leptophilicmutau_2muZp039_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "042": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100013.MGPy8EG_30nlo_Leptophilicmutau_2muZp042_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "045": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100014.MGPy8EG_30nlo_Leptophilicmutau_2muZp045_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "048": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100015.MGPy8EG_30nlo_Leptophilicmutau_2muZp048_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "051": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100016.MGPy8EG_30nlo_Leptophilicmutau_2muZp051_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "054": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100017.MGPy8EG_30nlo_Leptophilicmutau_2muZp054_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "057": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100018.MGPy8EG_30nlo_Leptophilicmutau_2muZp057_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "060": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100019.MGPy8EG_30nlo_Leptophilicmutau_2muZp060_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "063": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100020.MGPy8EG_30nlo_Leptophilicmutau_2muZp063_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "066": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100021.MGPy8EG_30nlo_Leptophilicmutau_2muZp066_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "069": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100022.MGPy8EG_30nlo_Leptophilicmutau_2muZp069_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "072": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100023.MGPy8EG_30nlo_Leptophilicmutau_2muZp072_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "075": "/net/s3_datab/daits/g21.6.56/evgen/mc15.100024.MGPy8EG_30nlo_Leptophilicmutau_2muZp075_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
}
norm_factor_path = "fit_results/fit_norm.txt"

def create_JO(mass, sample, xsec, sub_folder):
    job_name = "Zp{}.py".format(mass)
    file = open(sub_folder + "/" + job_name, "w+")
    template_cfg = """
import os, glob
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AlgSequence import AlgSequence
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = -1

svcMgr.EventSelector.InputCollections = glob.glob("{fsample}/*EVNT*")

job = AlgSequence()

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD'] + '/..'

rivet.Analyses += ["Z4mu"]
rivet.RunName = ''
rivet.HistoFile = 'Zp{fmass}_out_.yoda.gz'
rivet.CrossSection = {fxsec}
#rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True
job += rivet

    """
    file.write(template_cfg.format(fmass=mass, fsample=sample, fxsec=xsec))
    file.close()

def load_norm_factors(input_file_path, norm_to_xsec=False):
    norm_dict = {}
    with open(input_file_path) as norm_input:
        for line in norm_input:
            line = line.strip()
            if line.startswith("#") or line.startswith("//"):
                continue
            else:
                new_input_line = line.split()
                mass = float(new_input_line[0])
                if norm_to_xsec:
                    norm = float(new_input_line[2])
                else:
                    norm = float(new_input_line[1])
                if mass in norm_dict:
                    raise KeyError("Duplicated mass found, please check input norm factor file.")
                else:
                    norm_dict[mass] = norm
    return norm_dict

if __name__ == "__main__":
    if len(sys.argv) > 1:
        run_folder = sys.argv[1]
    else:
        run_folder = "./"
    norm_dict = load_norm_factors(norm_factor_path, norm_to_xsec=True)
    for mass in sample_lists:
        sub_folder = run_folder + "Zp" + mass
        if not os.path.exists(sub_folder):
            os.makedirs(sub_folder)
        sample = sample_lists[mass]
        create_JO(mass, sample, norm_dict[float(mass)], sub_folder)

    # create scale script
    """ doesn't work, use plot scale option instead
    with open("yoda_scale_from_1.sh", "w") as scale_from_1:
        for mass in sample_lists:
            sub_folder = run_folder + "Zp" + mass
            scale_from_1.write("cd {}\n".format(sub_folder))
            scale_from_1.write("yodamerge -o Zp{fmass}_out_normed.yoda.gz Zp{fmass}_out_.yoda.gz:{fscale}\n".format(fmass=mass, fscale=norm_dict[float(mass)]))
            scale_from_1.write("cd ..\n")
    """
