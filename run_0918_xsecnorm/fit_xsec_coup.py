import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os

lumi = 139.0
norm_coup = 0.008  # coupling value to be normalized to
input_file = "Leptophilicmutau_2muZpxxx_4mu_4pt2_couplingsXS.txt"

fit_data = {}
with open(input_file) as fit_input:
    print("Reading data...")
    for line in fit_input:
        line = line.strip()
        if line.startswith("#") or line.startswith("//"):
            continue
        else:
            new_input_line = line.split()
            mass = float(new_input_line[0])
            coup = float(new_input_line[1])
            xsec_py = float(new_input_line[2])
            xsec_md = float(new_input_line[3])
            xsec = (xsec_py + xsec_md) / 2.0
            if coup > 0 :
                if mass in fit_data:
                    fit_data[mass].append((coup, xsec))
                else:
                    fit_data[mass] = [(coup, xsec)]
    print("Fitting parameters p0, p1, p2...")
    if not os.path.exists("./fit_results"):
        os.makedirs("./fit_results")
    norm_factors = ["# mass normed_xsec/original_xsec normed_xsec normed_xsec*lumi"]
    for mass_point in fit_data:
        #print("processing mass:", str(mass_point))

        fit_points = np.array(fit_data[mass_point])
        x = fit_points[:, 0]
        y = fit_points[:, 1]

        # calculate polynomial
        z = np.polyfit(x, y, 5)
        f = np.poly1d(z)

        z_inver = np.polyfit(y, x, 5)
        #print("poly_fit:", list(z_inver))
        print(int(mass_point), ":", list(z_inver)[::-1], ",")

        # calculate new x's and y's
        x_new = np.linspace(x[0], x[-1], 50)
        y_new = f(x_new)
        fig, ax = plt.subplots(figsize=(8, 6))
        ax.set_title("Z' mass = {} GeV".format(mass_point))
        ax.set_xlabel("coupling")
        ax.set_ylabel("cross-section [fb]")
        ax.set_xlim([min(x), max(x)])
        ax.plot(x, y, "o", x_new, y_new)
        ax.text(
            0.05,
            0.95,
            "$y={c0:+.5e}$\n ${c1:+.5e} x$\n ${c2:+.5e} x^2$".format(
                c0=z[2], c1=z[1], c2=z[0]
            ),
            verticalalignment="top",
            horizontalalignment="left",
            transform=ax.transAxes,
            color="green",
            fontsize=15,
        )
        fig.savefig("fit_results/mass_{}.png".format(mass_point), dpi=fig.dpi)
        plt.close(fig)

        # calculate norm factor
        original_xsec = f(
            max(x)
        )  # original xsec use the largest coupling factor in fit_input
        normed_xsec = f(norm_coup)
        norm_factor = normed_xsec / original_xsec
        norm_factors.append(
            "{} {} {} {}".format(
                mass_point, norm_factor, normed_xsec, normed_xsec * lumi
            )
        )

    with open("fit_results/fit_norm.txt", "w") as out_file:
        for line in norm_factors:
            out_file.write(line + "\n")
