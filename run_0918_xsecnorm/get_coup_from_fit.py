import matplotlib as mpl

mpl.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import os

lumi = 139.0
norm_coup = 0.008  # coupling value to be normalized to
input_file = "Leptophilicmutau_2muZpxxx_4mu_4pt2_couplingsXS.txt"

#input_limits_file = "limit_list_1029.txt"
input_limits_file = "limit_list_1029_dnn.txt"
#output_coups_file = "coup_list_1029.txt"
output_coups_file = "coup_list_1029_dnn.txt"

# get fitting functions
fit_data = {}
function_dict = {}
with open(input_file) as fit_input:
    print("Reading data...")
    for line in fit_input:
        line = line.strip()
        if line.startswith("#") or line.startswith("//"):
            continue
        else:
            new_input_line = line.split()
            mass = float(new_input_line[0])
            coup = float(new_input_line[1])
            xsec_py = float(new_input_line[2])
            xsec_md = float(new_input_line[3])
            xsec = (xsec_py + xsec_md) / 2.0
            if mass in fit_data:
                fit_data[mass].append((coup, xsec))
            else:
                fit_data[mass] = [(coup, xsec)]
    print("Fitting...")
    if not os.path.exists("./fit_results"):
        os.makedirs("./fit_results")
    norm_factors = ["# mass normed_xsec/original_xsec normed_xsec normed_xsec*lumi"]
    for mass_point in fit_data:
        print("processing mass:", str(mass_point))

        fit_points = np.array(fit_data[mass_point])
        x = fit_points[:, 0]
        y = fit_points[:, 1]

        # calculate polynomial
        # NOTE: fit coup as a function of XSec, this is different from fit_xsec_coup.py
        z = np.polyfit(y, x, 5)
        print("paras:", z)
        f = np.poly1d(z)
        function_dict[mass_point] = f

# calculate g for given limits
output_line_mass = "mass "
output_line0 = "medium "
output_line1 = "+sigma "
output_line2 = "+2sigma "
output_line3 = "-sigma "
output_line4 = "-2sigma "
with open(input_limits_file) as input_limits:
    print("Reading input limits...")
    for line in input_limits:
        line = line.strip()
        if line.startswith("#") or line.startswith("//"):
            continue
        else:
            new_input_line = line.split()
            mass = float(new_input_line[0])
            limit_medium = float(new_input_line[1])
            limit_p_sigma = float(new_input_line[2])
            limit_p_2sigma = float(new_input_line[3])
            limit_m_sigma = float(new_input_line[4])
            limit_m_2sigma = float(new_input_line[5])
            f_xsec_coup = function_dict[mass]
            coup_medium = f_xsec_coup(limit_medium)
            coup_p_sigma = f_xsec_coup(limit_p_sigma)
            coup_p_2sigma = f_xsec_coup(limit_p_2sigma)
            coup_m_sigma = f_xsec_coup(limit_m_sigma)
            coup_m_2sigma = f_xsec_coup(limit_m_2sigma)
            print("mass =", mass)
            print(limit_medium, "->", coup_medium)
            print(limit_p_sigma, "->", coup_p_sigma)
            print(limit_p_2sigma, "->", coup_p_2sigma)
            print(limit_m_sigma, "->", coup_m_sigma)
            print(limit_m_2sigma, "->", coup_m_2sigma)
            output_line_mass += ", {}".format(mass)
            output_line0 += ", {}".format(coup_medium)
            output_line1 += ", {}".format(coup_p_sigma)
            output_line2 += ", {}".format(coup_p_2sigma)
            output_line3 += ", {}".format(coup_m_sigma)
            output_line4 += ", {}".format(coup_m_2sigma)

with open(output_coups_file, "w") as out_coup_file:
    out_coup_file.write(output_line_mass + "\n")
    out_coup_file.write(output_line0 + "\n")
    out_coup_file.write(output_line1 + "\n")
    out_coup_file.write(output_line2 + "\n")
    out_coup_file.write(output_line3 + "\n")
    out_coup_file.write(output_line4 + "\n")

