# Cross-section related calculations

## fit xsec-coupling relatio

```bash
python fit_xsec_coup.py
```

the code use Leptophilicmutau_2muZpxxx_4mu_4pt2_couplingsXS.txt as input

## calculate coupling with given xsec

used for plotting "brazilian plot" for coupling

```bash
python get_coup_from_fit.py
```

* the code use limit_list_1029.txt as input, or change the settings in the code.
* the output can be used to make "brazilian" plots on coupling