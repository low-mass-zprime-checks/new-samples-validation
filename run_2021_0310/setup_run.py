import os
import sys

sample_lists = {
    "005": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988400.MGPy8EG_30nlo_Leptophilicmutau_muvZp005_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "007": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988401.MGPy8EG_30nlo_Leptophilicmutau_muvZp007_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "009": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988402.MGPy8EG_30nlo_Leptophilicmutau_muvZp009_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "010": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988490.MGPy8EG_30nlo_Leptophilicmutau_muvZp010_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "011": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988403.MGPy8EG_30nlo_Leptophilicmutau_muvZp011_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "013": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988404.MGPy8EG_30nlo_Leptophilicmutau_muvZp013_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "015": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988405.MGPy8EG_30nlo_Leptophilicmutau_muvZp015_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "017": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988406.MGPy8EG_30nlo_Leptophilicmutau_muvZp017_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "019": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988407.MGPy8EG_30nlo_Leptophilicmutau_muvZp019_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "023": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988408.MGPy8EG_30nlo_Leptophilicmutau_muvZp023_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "027": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988409.MGPy8EG_30nlo_Leptophilicmutau_muvZp027_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "031": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988410.MGPy8EG_30nlo_Leptophilicmutau_muvZp031_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "035": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988411.MGPy8EG_30nlo_Leptophilicmutau_muvZp035_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "039": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988412.MGPy8EG_30nlo_Leptophilicmutau_muvZp039_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "042": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988413.MGPy8EG_30nlo_Leptophilicmutau_muvZp042_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "045": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988414.MGPy8EG_30nlo_Leptophilicmutau_muvZp045_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "048": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988415.MGPy8EG_30nlo_Leptophilicmutau_muvZp048_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "051": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988416.MGPy8EG_30nlo_Leptophilicmutau_muvZp051_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "054": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988417.MGPy8EG_30nlo_Leptophilicmutau_muvZp054_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "057": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988418.MGPy8EG_30nlo_Leptophilicmutau_muvZp057_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "060": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988419.MGPy8EG_30nlo_Leptophilicmutau_muvZp060_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "063": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988420.MGPy8EG_30nlo_Leptophilicmutau_muvZp063_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "066": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988421.MGPy8EG_30nlo_Leptophilicmutau_muvZp066_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "069": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988422.MGPy8EG_30nlo_Leptophilicmutau_muvZp069_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "072": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988423.MGPy8EG_30nlo_Leptophilicmutau_muvZp072_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
    "075": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988424.MGPy8EG_30nlo_Leptophilicmutau_muvZp075_3muv_3pt2.evgen.EVNT.e8304_pUM999999",
}
norm_factor_path = "fit_results/fit_norm.txt"


def create_JO(mass, sample, sub_folder):
    job_name = "Zp{}.py".format(mass)
    file = open(sub_folder + "/" + job_name, "w+")
    template_cfg = """
import os, glob
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AlgSequence import AlgSequence
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = -1

svcMgr.EventSelector.InputCollections = glob.glob("{fsample}/*EVNT*")

job = AlgSequence()

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']

rivet.Analyses += ["W3mu1nu"]
rivet.RunName = ''
rivet.HistoFile = 'Zp{fmass}_out_.yoda.gz'
rivet.CrossSection = 1.0
#rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True
job += rivet

    """
    file.write(template_cfg.format(fmass=mass, fsample=sample))
    file.close()


def creat_ana_code(mass, sub_folder):
    with open("W3mu1nu_Analysis_template.cc", "r") as ana_code_file:
        ana_code = ana_code_file.read()
        modified_ana_code = ana_code.replace(
            "ZPRIME_MASS_DEF", "ZPRIME_MASS {}".format(float(mass))
        )
        with open(sub_folder + "/" + "W3mu1nu_Analysis.cc", "w+") as out_code_file:
            out_code_file.write(modified_ana_code)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        run_folder = sys.argv[1]
    else:
        run_folder = "./"
    for mass in sample_lists:
        sub_folder = run_folder + "Zp" + mass
        if not os.path.exists(sub_folder):
            os.makedirs(sub_folder)
        sample = sample_lists[mass]
        create_JO(mass, sample, sub_folder)
        creat_ana_code(mass, sub_folder)
