// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/PrimaryParticles.hh"
#define ZPRIME_MASS_DEF // GeV, value need to be assigned in setup_run.py

namespace Rivet
{

    /// ATLAS 4-lepton lineshape at 13 TeV
    class W3mu1nu : public Analysis
    {
    public:
        W3mu1nu() : Analysis("W3mu1nu") {}

        void init()
        {
            PromptFinalState photons(Cuts::abspid == PID::PHOTON);
            PromptFinalState muons(Cuts::abspid == PID::MUON);
            PromptFinalState nus(Cuts::abspid == 14);
            Cut mu_fid_sel = (Cuts::abseta < 2.7) && (Cuts::pT > 3 * GeV);
            DressedLeptons dressed_muons(photons, muons, 0.005, mu_fid_sel, false);
            declare(dressed_muons, "muons");
            declare(nus, "nus");

            // Book histograms
            book(m_hists["mu1_pt"], "mu1_pt", 40, 0, 200);
            book(m_hists["mu2_pt"], "mu2_pt", 40, 0, 100);
            book(m_hists["mu3_pt"], "mu3_pt", 40, 0, 100);
            book(m_hists["nu_pt"], "nu_pt", 40, 0, 100);
            book(m_hists["mu1_eta"], "mu1_eta", 40, -3.5, 3.5);
            book(m_hists["mu2_eta"], "mu2_eta", 40, -3.5, 3.5);
            book(m_hists["mu3_eta"], "mu3_eta", 40, -3.5, 3.5);
            book(m_hists["nu_eta"], "nu_eta", 40, -3.5, 3.5);
            book(m_hists["mu1_phi"], "mu1_phi", 40, 0, TWOPI);
            book(m_hists["mu2_phi"], "mu2_phi", 40, 0, TWOPI);
            book(m_hists["mu3_phi"], "mu3_phi", 40, 0, TWOPI);
            book(m_hists["nu_phi"], "nu_phi", 40, 0, TWOPI);
            book(m_hists["num_mu"], "num_mu", 10, 0, 10);

            book(m_hists["zprime_m_truth"], "zprime_m_truth", 50, 0, 100);

            book(m_hists["sf_mu1_nu_mt"], "sf_mu1_nu_mt", 40, 0, 100);
            book(m_hists["sf_mu2_nu_mt"], "sf_mu2_nu_mt", 40, 0, 100);
            book(m_hists["sf_mu1_alone_mu_delta_R"], "sf_mu1_alone_mu_delta_R", 40, 0, 10);
            book(m_hists["sf_mu2_alone_mu_delta_R"], "sf_mu2_alone_mu_delta_R", 40, 0, 10);

            book(m_hists["zprime_pair_m"], "zprime_pair_m", 50, 0, 100);

            book(m_hists["passed_events_count"], "passed_events_count", 2, 0, 2);
        }

        // Do the analysis
        void analyze(const Event &event)
        {
            // preselection of leptons for Z' -> 4mu final states
            Particles dressed_muons;
            for (auto lep : apply<DressedLeptons>(event, "muons").dressedLeptons())
            {
                dressed_muons.push_back(lep);
            }
            Particles nus;
            for (auto nu : apply<PromptFinalState>(event, "nus").particles(Cuts::abspid == 14))
            {
                nus.push_back(nu);
            }
            Particles zprimes;
            for (ConstGenParticlePtr p : HepMCUtils::particles(event.genEvent()))
            {
                if (abs(p->pdg_id()) == 999888)
                {
                    zprimes.push_back(Particle(p));
                }
            }

            // select candidates
            auto final_col = getParticlesCollection(dressed_muons, nus);
            if (final_col.empty())
            {
                m_hists["passed_events_count"]->fill(0);
                vetoEvent;
            }
            else
            {
                m_hists["passed_events_count"]->fill(1);
            }

            // fill histograms
            if (zprimes.size() > 0)
            {
                m_hists["zprime_m_truth"]->fill(zprimes[0].mass());
                m_hists["num_mu"]->fill(dressed_muons.size());

                m_hists["mu1_pt"]->fill(final_col[0].get_mu1().pT() / GeV);
                m_hists["mu2_pt"]->fill(final_col[0].get_mu2().pT() / GeV);
                m_hists["mu3_pt"]->fill(final_col[0].get_mu3().pT() / GeV);
                m_hists["nu_pt"]->fill(final_col[0].get_nu().pT() / GeV);
                m_hists["mu1_eta"]->fill(final_col[0].get_mu1().eta());
                m_hists["mu2_eta"]->fill(final_col[0].get_mu2().eta());
                m_hists["mu3_eta"]->fill(final_col[0].get_mu3().eta());
                m_hists["nu_eta"]->fill(final_col[0].get_nu().eta());
                m_hists["mu1_phi"]->fill(final_col[0].get_mu1().phi());
                m_hists["mu2_phi"]->fill(final_col[0].get_mu2().phi());
                m_hists["mu3_phi"]->fill(final_col[0].get_mu3().phi());
                m_hists["nu_phi"]->fill(final_col[0].get_nu().phi());
                FourMomentum sf_mu1_nu_mom;
                FourMomentum sf_mu2_nu_mom;
                sf_mu1_nu_mom = final_col[0].getSameSignMuonPair().first.momentum() + final_col[0].get_nu().momentum();
                sf_mu2_nu_mom = final_col[0].getSameSignMuonPair().second.momentum() + final_col[0].get_nu().momentum();
                m_hists["sf_mu1_nu_mt"]->fill(sqrt(sf_mu1_nu_mom.mass2() + sf_mu1_nu_mom.pT2()));
                m_hists["sf_mu2_nu_mt"]->fill(sqrt(sf_mu2_nu_mom.mass2() + sf_mu2_nu_mom.pT2()));
                m_hists["zprime_pair_m"]->fill(final_col[0].getOppoSignMuonPair().mom().mass());

                FourMomentum delata_sf_mu1_alone_mu_mom;
                FourMomentum delata_sf_mu2_alone_mu_mom;
                delata_sf_mu1_alone_mu_mom = final_col[0].getSameSignMuonPair().first.momentum() - final_col[0].get_alone_mu().momentum();
                delata_sf_mu2_alone_mu_mom = final_col[0].getSameSignMuonPair().second.momentum() - final_col[0].get_alone_mu().momentum();
                m_hists["sf_mu1_alone_mu_delta_R"]->fill(sqrt(pow(delata_sf_mu1_alone_mu_mom.phi(), 2) + pow(delata_sf_mu1_alone_mu_mom.eta(), 2)));
                m_hists["sf_mu2_alone_mu_delta_R"]->fill(sqrt(pow(delata_sf_mu2_alone_mu_mom.phi(), 2) + pow(delata_sf_mu2_alone_mu_mom.eta(), 2)));
            }
        }

        /// Finalize
        void finalize()
        {
            // const double sf = 1 / sumOfWeights();
            // scale(m_hists, sf);
            normalize(m_hists);
        }

        struct Dilepton : public ParticlePair
        {
            Dilepton() {}
            Dilepton(ParticlePair _particlepair) : ParticlePair(_particlepair)
            {
                assert(first.abspid() == second.abspid());
            }
            FourMomentum mom() const { return first.momentum() + second.momentum(); }
            FourMomentum delta_mom() const { return first.momentum() - second.momentum(); }
            operator FourMomentum() const { return mom(); }
            static bool cmppT(const Dilepton &lx, const Dilepton &rx) { return lx.mom().pT() < rx.mom().pT(); }
            int flavour() const { return first.abspid(); }
            double pTl1() const { return first.pT(); }
            double pTl2() const { return second.pT(); }
            double dR() const { return deltaR(first.momentum(), second.momentum()); }
        };

        struct Collection3Mu1nu
        {
            Collection3Mu1nu(Particle mu1, Particle mu2, Particle mu3, Dilepton same_sign_muon_pair, Particle alone_muon, Dilepton oppo_sign_muon_pair, Particle muon_from_w, Particle muon_nu)
            {
                _mu_1 = mu1;
                _mu_2 = mu2;
                _mu_3 = mu3;
                _muon_nu = muon_nu;
                _same_sign_muon_pair = same_sign_muon_pair;
                _alone_muon = alone_muon;
                _oppo_sign_muon_pair = oppo_sign_muon_pair;
                _muon_from_w = muon_from_w;
            }
            FourMomentum mom() const { return _same_sign_muon_pair.mom() + _alone_muon.mom() + _muon_nu.mom(); }
            Dilepton getSameSignMuonPair() const { return _same_sign_muon_pair; }
            Dilepton getOppoSignMuonPair() const { return _oppo_sign_muon_pair; }
            Particle get_alone_mu() const { return _alone_muon; }
            Particle get_w_mu() const { return _muon_from_w; }
            Particle get_mu1() const { return _mu_1; }
            Particle get_mu2() const { return _mu_2; }
            Particle get_mu3() const { return _mu_3; }
            Particle get_nu() const { return _muon_nu; }
            Dilepton _same_sign_muon_pair;
            Particle _alone_muon;
            Dilepton _oppo_sign_muon_pair;
            Particle _muon_from_w;
            Particle _mu_1;
            Particle _mu_2;
            Particle _mu_3;
            Particle _muon_nu;
        };

        vector<Collection3Mu1nu> getParticlesCollection(Particles &muons, Particles &muon_nus)
        {
            vector<Collection3Mu1nu> collections;

            size_t n_muons = muons.size();
            size_t n_nus = muon_nus.size();
            if (n_muons < 3 || n_nus < 1)
                return collections;

            // Step 1: select 3 muons with highes pT
            std::sort(muons.begin(), muons.end(),
                      [](const Particle &p1, const Particle &p2) { return p1.pT() > p2.pT(); });
            Particle muon_0 = muons[0];
            Particle muon_1 = muons[1];
            Particle muon_2 = muons[2];
            if ((muon_0.pid() == muon_1.pid()) && (muon_1.pid() == muon_2.pid()))
            {
                return collections;
            }

            // Step 2: find the same sign pair
            Dilepton same_sign_pair;
            Particle alone_muon;
            if (muon_0.pid() == muon_1.pid())
            {
                same_sign_pair = Dilepton(make_pair(muon_0, muon_1));
                alone_muon = muon_2;
            }
            else if (muon_1.pid() == muon_2.pid())
            {
                same_sign_pair = Dilepton(make_pair(muon_1, muon_2));
                alone_muon = muon_0;
            }
            else
            {
                same_sign_pair = Dilepton(make_pair(muon_0, muon_2));
                alone_muon = muon_1;
            }

            // Step 3; find the radiated muon pair (closest mass to Z')
            Dilepton zprime_muon_pair;
            Particle w_muon;
            Dilepton muon_pair1;
            Dilepton muon_pair2;
            if (alone_muon.pT() > same_sign_pair.first.pT())
                muon_pair1 = Dilepton(make_pair(alone_muon, same_sign_pair.first));
            else
                muon_pair1 = Dilepton(make_pair(same_sign_pair.first, alone_muon));
            if (alone_muon.pT() > same_sign_pair.second.pT())
                muon_pair2 = Dilepton(make_pair(alone_muon, same_sign_pair.second));
            else
                muon_pair2 = Dilepton(make_pair(same_sign_pair.second, alone_muon));

            if (fabs(muon_pair1.mom().mass() - ZPRIME_MASS) < fabs(muon_pair2.mom().mass() - ZPRIME_MASS))
            {
                zprime_muon_pair = muon_pair1;
                w_muon = zprime_muon_pair.second;
            }
            else
            {
                zprime_muon_pair = muon_pair2;
                w_muon = zprime_muon_pair.first;
            }

            // STEP 4: select the highest pT nu
            std::sort(muon_nus.begin(), muon_nus.end(),
                      [](const Particle &p1, const Particle &p2) { return p1.pT() > p2.pT(); });

            // STEP 5: add final states collections
            collections.push_back(Collection3Mu1nu(muon_0, muon_1, muon_2, same_sign_pair, alone_muon, zprime_muon_pair, w_muon, muon_nus[0]));

            return collections;
        }

    private:
        map<string, Histo1DPtr> m_hists;
        static constexpr double W_mass = 80.379;
    };

    DECLARE_RIVET_PLUGIN(W3mu1nu);

} // namespace Rivet