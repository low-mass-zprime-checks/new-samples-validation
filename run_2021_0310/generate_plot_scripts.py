norm_factor_path = "fit_results/fit_norm.txt"


def load_norm_factors(input_file_path):
    norm_dict = {}
    with open(input_file_path) as norm_input:
        for line in norm_input:
            line = line.strip()
            if line.startswith("#") or line.startswith("//"):
                continue
            else:
                new_input_line = line.split()
                mass = float(new_input_line[0])
                norm = float(new_input_line[3])  # xsec * lumi
                if mass in norm_dict:
                    raise KeyError(
                        "Duplicated mass found, please check input norm factor file."
                    )
                else:
                    norm_dict[mass] = norm
    return norm_dict


if __name__ == "__main__":
    norm_dict = load_norm_factors(norm_factor_path)
    with open("make_plots_all.sh", "w") as plot_all_script:
        cmd_template = 'rivet-mkhtml --no-ratio --errs -o my_plots_Zp{fmass} Zp{fmass}/Zp{fmass}_out_.yoda.gz:"Title=Zp{fmass}":"Scale={fscale}"\n'
        for mass in norm_dict:
            mass_str = str(int(mass)).zfill(3)
            plot_all_script.write(
                cmd_template.format(fmass=mass_str, fscale=norm_dict[mass])
            )
    with open("make_plots_6points.sh", "w") as plot_6points:
        plot_6points.write("rivet-mkhtml --no-ratio --errs -o my_plots_6points ")
        for mass in ["005", "015", "035", "045", "060", "075"]:
            plot_6points.write(
                'Zp{fmass}/Zp{fmass}_out_.yoda.gz:"Title=Zp{fmass}":"Scale={fscale}" '.format(
                    fmass=mass, fscale=norm_dict[float(mass)]
                )
            )
    
    with open("make_plots_low_high.sh", "w") as plot_6points:
        plot_6points.write("rivet-mkhtml --no-ratio --errs -o my_plots_low ")
        for mass in ["005", "010", "015", "023", "035"]:
            plot_6points.write(
                'Zp{fmass}/Zp{fmass}_out_.yoda.gz:"Title=Zp{fmass}":"Scale={fscale}" '.format(
                    fmass=mass, fscale=norm_dict[float(mass)]
                )
            )
        plot_6points.write("\n\n")
        plot_6points.write("rivet-mkhtml --no-ratio --errs -o my_plots_high ")
        for mass in ["042", "051", "060", "069", "075"]:
            plot_6points.write(
                'Zp{fmass}/Zp{fmass}_out_.yoda.gz:"Title=Zp{fmass}":"Scale={fscale}" '.format(
                    fmass=mass, fscale=norm_dict[float(mass)]
                )
            )
    
