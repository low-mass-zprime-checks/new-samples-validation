for work_folder in Zp*/; do
    cd $work_folder
    rivet-build Rivet_W3mu1nu_Analysis.so W3mu1nu_Analysis.cc
    find_JO=$(find . -type f -name "Zp*.py")
    athena ${find_JO} >/dev/null 2>&1 &
    cd ..
done
