for work_folder in Zp*/; do
    cd $work_folder
    rivet-build Rivet_Z4mu_Analysis.so Z4mu_Analysis.cc
    cd ..
done