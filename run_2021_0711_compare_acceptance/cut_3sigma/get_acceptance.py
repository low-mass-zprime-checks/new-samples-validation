import csv
import glob
import os
import sys
from array import array

import yoda


names = list()
accept_list = list()

for yoda_path in sorted(glob.glob("*/*.yoda.gz")):
    print "Checking:", yoda_path

    yodaAOs = yoda.read(yoda_path)
    for name in yodaAOs:
        yodaAO = yodaAOs[name]
        rtAO = None
        if "Histo1D" in str(yodaAO):
            if (
                "passed" in str(yodaAO)
                and "RAW" not in str(yodaAO)
                and "Zp010" not in str(yoda_path)
            ):
                evnt_cut = yodaAO.bin(0).sumW()
                evnt_pass = yodaAO.bin(1).sumW()
                accept = evnt_pass / (evnt_pass + evnt_cut)
                names.append(yoda_path.split("/")[0])
                accept_list.append(accept)
                print "accept = ", accept

print (names)
print (accept_list)

with open('accepts.csv', 'w') as f:
      
    # using csv.writer method from CSV package
    write = csv.writer(f)
      
    write.writerow(names)
    write.writerow(accept_list)
