for work_folder in Zp*/; do
    cd $work_folder
    find_JO=$(find . -type f -name "Zp*.py")
    echo "run ${find_JO}"
    athena ${find_JO} &> run.log &
    cd ..
done
