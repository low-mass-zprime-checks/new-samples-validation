
rivet-mkhtml --no-ratio --errs -o my_plots_Zp069_2mu2tau Zp069_2mu2tau/Zp069_2mu2tau_out_.yoda.gz:"Title=Zp069_2mu2tau"
rivet-mkhtml --no-ratio --errs -o my_plots_Zp072_2mu2tau Zp072_2mu2tau/Zp072_2mu2tau_out_.yoda.gz:"Title=Zp072_2mu2tau"
rivet-mkhtml --no-ratio --errs -o my_plots_Zp075_2mu2tau Zp075_2mu2tau/Zp075_2mu2tau_out_.yoda.gz:"Title=Zp075_2mu2tau"

rivet-mkhtml --no-ratio --errs -o my_plots_Zp069_2muZp Zp069_2muZp/Zp069_2muZp_out_.yoda.gz:"Title=Zp069_2muZp"
rivet-mkhtml --no-ratio --errs -o my_plots_Zp072_2muZp Zp072_2muZp/Zp072_2muZp_out_.yoda.gz:"Title=Zp072_2muZp"
rivet-mkhtml --no-ratio --errs -o my_plots_Zp075_2muZp Zp075_2muZp/Zp075_2muZp_out_.yoda.gz:"Title=Zp075_2muZp"

rivet-mkhtml --no-ratio --errs -o my_plots_Zp069_2tauZp Zp069_2tauZp/Zp069_2tauZp_out_.yoda.gz:"Title=Zp069_2tauZp"
rivet-mkhtml --no-ratio --errs -o my_plots_Zp072_2tauZp Zp072_2tauZp/Zp072_2tauZp_out_.yoda.gz:"Title=Zp072_2tauZp"
rivet-mkhtml --no-ratio --errs -o my_plots_Zp075_2tauZp Zp075_2tauZp/Zp075_2tauZp_out_.yoda.gz:"Title=Zp075_2tauZp"
