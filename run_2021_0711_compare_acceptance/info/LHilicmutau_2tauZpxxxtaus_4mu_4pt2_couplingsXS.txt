//
// Leptophilic interaction with different Z' mass from process pp --> 2tau+Zp --> 2tau+2tau --> 4mu around Z peak
// under muon-tau model, where Z' could decay into muon pair, muon neutrino pair, tau pair and tau neutrino pair
// All coupling constants are same for left-hand muon, tau and their neutrinos, for righ-hand muon and tau.
// No couplings with electron and its neutrino under muon-tau model, details with an example:
//   Zp mass 5.0GeV with gzpmul = 0.008
//   couplings:
//   1    0.0  #  gzpel
//   2    0.0  #  gzper
//   3    0.008  #  gzpmul
//   4    0.008  #  gzpmur = gzpmul
//   5    0.008  #  gzptal = gzpmul
//   6    0.008  #  gzptar = gzpmul
//
// with Zp decays
// #      PDG        Width
// DECAY  999888   Auto
// #  BR             NDA  ID1    ID2   ...
//    3.333332e-01   2    13  -13 # without consider phase-space difference due mass diff.
//    1.667668e-01   2    14  -14 # without consider phase-space difference due mass diff.
//    3.333332e-01   2    15  -15 # without consider phase-space difference due mass diff.
//    1.667888e-01   2    16  -16 # without consider phase-space difference due mass diff.
// Note: the Zp decay width is set to "Auto" for program to re-calculate it with right phase space under
//       new settings, such as different Zp mass.
//
// Without consider phase space due to lepton mass, the
// 33.33 Z' decays into muon pair, 16.67% Z' decays to muon neutrion pair and
// 33.33 Z' decays into tau pair,  16.67% Z' decays to tau neutrion pair.
//
// MadGraph PDF set : NNPDF30nlo
// Pythia8 PDF set  : NNPDF23LO of A14 tune
// 
// No cut at parton level:
// Filter at event generation level : MultiMuonFiter
//                                    4 muons with it pt > 2.0GeV and |eta| < 3.0
//
//
// Data format
// ZpMass(GeV) Muon/Muon-neutrinoCouplings Cross-sectionByPythia8(fb) Cross-sectionByMadgraph(fb) FilterEffic +- FilterEErr BR(Z'->2tau)
//
// ZpMass                      : Mass of Z' in GeV
// Muon/Muon-neutrinoCouplings : Z' coupling constant with muon or muon neutrion
// Cross-sectionByPythia8      : Cross-section from Pythia8 after event generation in fb
// Cross-sectionByMadgraph     : Cross-section from Madgraph at parton level in fb
// FilterEffi                  : event filter efficiency
// FilterEErr                  : Statistic error on event filter efficiency 
// BR(Z'->2tau)                : Branching ratio of Z'->2tau
//
// Note : 
//  1) Cross-sectionByPythia8 and Cross-sectionByMadgraph are same in case no parton level event removed by Pythia8
//  2) Following cross-sections are for muon-tau model with Z'->2tau branching ratio 33.33%. The cross-section must
//     be corrected according to  Z'->2tau branching ratio if MC model is changed.
//  3) tau is forced to decay into muon, i.e., tau -> mu + mu_neutrino + tau_neutrino,	
//     but the cross-section does not include branching ratio of tau->mu+mu_neutrino+tau_neutrino.
//     The final cross-section must be corrected by Br(tau->mu+mu_neutrino+tau_neutrino)**4
//
// Cross-sections are for 25 mass points used for MC singal sample production plus a 10GeV Z' mass point
//
5.0   0.008   8.0621   8.06217   0.0894535 +- 0.000155843   0.30582
7.0   0.0085   6.5477   6.5475   0.0929097 +- 0.000161557   0.32718
9.0   0.009   5.36847   5.3683   0.102659 +- 0.000177548   0.331181
11.0   0.0095   4.51603   4.51593   0.114883 +- 0.000197331   0.33239
13.0   0.01   3.8645   3.8643   0.127719 +- 0.000217783   0.332859
15.0   0.012   4.36527   4.3653   0.140245 +- 0.000237418   0.333069
17.0   0.014   4.71367   4.71377   0.152813 +- 0.000256797   0.33317
19.0   0.016   4.92427   4.92387   0.164939 +- 0.000275184   0.33323
23.0   0.024   7.1889   7.1886   0.186194 +- 0.000306666   0.333289
27.0   0.032   8.38043   8.37997   0.203945 +- 0.000332219   0.33331
31.0   0.04   8.5953   8.595   0.217922 +- 0.000351857   0.33332
35.0   0.06   12.6393   12.639   0.22919 +- 0.000367374   0.333321
39.0   0.08   14.546   14.5467   0.237606 +- 0.00037878   0.33333
42.0   0.09   13.1743   13.1727   0.242903 +- 0.000385876   0.33333
45.0   0.1   11.5353   11.5353   0.247311 +- 0.000391734   0.33333
48.0   0.11   9.7948   9.79417   0.250561 +- 0.000396024   0.33333
51.0   0.12   8.0941   8.09393   0.254688 +- 0.000401436   0.33333
54.0   0.16   9.88837   9.88847   0.257862 +- 0.000405573   0.33333
57.0   0.2   10.527   10.5263   0.265414 +- 0.000415322   0.33333
60.0   0.2665   12.66   12.66   0.275666 +- 0.000428343   0.33333
63.0   0.333   13.436   13.4363   0.292478 +- 0.000449162   0.33333
66.0   0.4   13.3683   13.3683   0.314542 +- 0.000475454   0.33333
69.0   0.467   12.933   12.9333   0.344748 +- 0.000509502   0.33333
72.0   0.5335   12.5513   12.5533   0.377182 +- 0.000543465   0.33333
75.0   0.6   12.4863   12.4867   0.406518 +- 0.000571772   0.33333
10.0   0.01   5.73963   5.73943   0.108449 +- 0.000186956   0.33194
