# run 2021 0711

## Contents

- compare event acceptance between 2muZ' & 2tauZ' samples
- check pp -> 2mu+Z' -> 2mu+2tau -> 4mu (2mu_2tau_4mu)

## Update

- new samples have name start with mc16.
