import os
import sys

sample_lists = {
    "005": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988200.MGPy8EG_30nlo_LHilicmutau_2muZp005taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "007": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988201.MGPy8EG_30nlo_LHilicmutau_2muZp007taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "009": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988202.MGPy8EG_30nlo_LHilicmutau_2muZp009taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "011": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988203.MGPy8EG_30nlo_LHilicmutau_2muZp011taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "013": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988204.MGPy8EG_30nlo_LHilicmutau_2muZp013taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "015": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988205.MGPy8EG_30nlo_LHilicmutau_2muZp015taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "017": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988206.MGPy8EG_30nlo_LHilicmutau_2muZp017taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "019": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988207.MGPy8EG_30nlo_LHilicmutau_2muZp019taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "023": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988208.MGPy8EG_30nlo_LHilicmutau_2muZp023taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "027": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988209.MGPy8EG_30nlo_LHilicmutau_2muZp027taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "031": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988210.MGPy8EG_30nlo_LHilicmutau_2muZp031taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "035": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988211.MGPy8EG_30nlo_LHilicmutau_2muZp035taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "039": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988212.MGPy8EG_30nlo_LHilicmutau_2muZp039taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "042": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988213.MGPy8EG_30nlo_LHilicmutau_2muZp042taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "045": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988214.MGPy8EG_30nlo_LHilicmutau_2muZp045taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "048": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988215.MGPy8EG_30nlo_LHilicmutau_2muZp048taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "051": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988216.MGPy8EG_30nlo_LHilicmutau_2muZp051taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "054": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988217.MGPy8EG_30nlo_LHilicmutau_2muZp054taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "057": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988218.MGPy8EG_30nlo_LHilicmutau_2muZp057taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "060": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988219.MGPy8EG_30nlo_LHilicmutau_2muZp060taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "063": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988220.MGPy8EG_30nlo_LHilicmutau_2muZp063taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "066": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988221.MGPy8EG_30nlo_LHilicmutau_2muZp066taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "069": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988222.MGPy8EG_30nlo_LHilicmutau_2muZp069taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "072": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988223.MGPy8EG_30nlo_LHilicmutau_2muZp072taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "075": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988224.MGPy8EG_30nlo_LHilicmutau_2muZp075taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "010": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988290.MGPy8EG_30nlo_LHilicmutau_2muZp010taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
}

def create_JO(mass, sample, sub_folder):
    job_name = "Zp{}.py".format(mass)
    file = open(sub_folder + "/" + job_name, "w+")
    template_cfg = """
import os, glob
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AlgSequence import AlgSequence
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = -1

svcMgr.EventSelector.InputCollections = glob.glob("{fsample}/*EVNT*")

job = AlgSequence()

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD'] + '/..'

rivet.Analyses += ["Z4mu"]
rivet.RunName = ''
rivet.HistoFile = 'Zp{fmass}_out_.yoda.gz'
#rivet.CrossSection =
#rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True
job += rivet

    """
    file.write(template_cfg.format(fmass=mass, fsample=sample))
    file.close()

if __name__ == "__main__":
    if len(sys.argv) > 1:
        run_folder = sys.argv[1]
    else:
        run_folder = "./"
    for mass in sample_lists:
        sub_folder = run_folder + "Zp" + mass
        if not os.path.exists(sub_folder):
            os.makedirs(sub_folder)
        sample = sample_lists[mass]
        create_JO(mass, sample, sub_folder)

    # create scale script
    """ doesn't work, use plot scale option instead
    with open("yoda_scale_from_1.sh", "w") as scale_from_1:
        for mass in sample_lists:
            sub_folder = run_folder + "Zp" + mass
            scale_from_1.write("cd {}\n".format(sub_folder))
            scale_from_1.write("yodamerge -o Zp{fmass}_out_normed.yoda.gz Zp{fmass}_out_.yoda.gz:{fscale}\n".format(fmass=mass, fscale=norm_dict[float(mass)]))
            scale_from_1.write("cd ..\n")
    """
