import os
import sys

sample_lists = {
    "005": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988250.MGPy8EG_30nlo_LHilicmutau_2tauZp005taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "007": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988251.MGPy8EG_30nlo_LHilicmutau_2tauZp007taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "009": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988252.MGPy8EG_30nlo_LHilicmutau_2tauZp009taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "011": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988253.MGPy8EG_30nlo_LHilicmutau_2tauZp011taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "013": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988254.MGPy8EG_30nlo_LHilicmutau_2tauZp013taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "015": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988255.MGPy8EG_30nlo_LHilicmutau_2tauZp015taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "017": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988256.MGPy8EG_30nlo_LHilicmutau_2tauZp017taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "019": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988257.MGPy8EG_30nlo_LHilicmutau_2tauZp019taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "023": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988258.MGPy8EG_30nlo_LHilicmutau_2tauZp023taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "027": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988259.MGPy8EG_30nlo_LHilicmutau_2tauZp027taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "031": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988260.MGPy8EG_30nlo_LHilicmutau_2tauZp031taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "035": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988261.MGPy8EG_30nlo_LHilicmutau_2tauZp035taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "039": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988262.MGPy8EG_30nlo_LHilicmutau_2tauZp039taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "042": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988263.MGPy8EG_30nlo_LHilicmutau_2tauZp042taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "045": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988264.MGPy8EG_30nlo_LHilicmutau_2tauZp045taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "048": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988265.MGPy8EG_30nlo_LHilicmutau_2tauZp048taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "051": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988266.MGPy8EG_30nlo_LHilicmutau_2tauZp051taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "054": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988267.MGPy8EG_30nlo_LHilicmutau_2tauZp054taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "057": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988268.MGPy8EG_30nlo_LHilicmutau_2tauZp057taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "060": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988269.MGPy8EG_30nlo_LHilicmutau_2tauZp060taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "063": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988270.MGPy8EG_30nlo_LHilicmutau_2tauZp063taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "066": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988271.MGPy8EG_30nlo_LHilicmutau_2tauZp066taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "069": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988272.MGPy8EG_30nlo_LHilicmutau_2tauZp069taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "072": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988273.MGPy8EG_30nlo_LHilicmutau_2tauZp072taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "075": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988274.MGPy8EG_30nlo_LHilicmutau_2tauZp075taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "010": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988295.MGPy8EG_30nlo_LHilicmutau_2tauZp010taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
}

def create_JO(mass, sample, sub_folder):
    job_name = "Zp{}.py".format(mass)
    file = open(sub_folder + "/" + job_name, "w+")
    template_cfg = """
import os, glob
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AlgSequence import AlgSequence
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = -1

svcMgr.EventSelector.InputCollections = glob.glob("{fsample}/*EVNT*")

job = AlgSequence()

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD'] + '/..'

rivet.Analyses += ["Z4mu"]
rivet.RunName = ''
rivet.HistoFile = 'Zp{fmass}_out_.yoda.gz'
#rivet.CrossSection =
#rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True
job += rivet

    """
    file.write(template_cfg.format(fmass=mass, fsample=sample))
    file.close()

if __name__ == "__main__":
    if len(sys.argv) > 1:
        run_folder = sys.argv[1]
    else:
        run_folder = "./"
    for mass in sample_lists:
        sub_folder = run_folder + "Zp" + mass
        if not os.path.exists(sub_folder):
            os.makedirs(sub_folder)
        sample = sample_lists[mass]
        create_JO(mass, sample, sub_folder)

    # create scale script
    """ doesn't work, use plot scale option instead
    with open("yoda_scale_from_1.sh", "w") as scale_from_1:
        for mass in sample_lists:
            sub_folder = run_folder + "Zp" + mass
            scale_from_1.write("cd {}\n".format(sub_folder))
            scale_from_1.write("yodamerge -o Zp{fmass}_out_normed.yoda.gz Zp{fmass}_out_.yoda.gz:{fscale}\n".format(fmass=mass, fscale=norm_dict[float(mass)]))
            scale_from_1.write("cd ..\n")
    """
