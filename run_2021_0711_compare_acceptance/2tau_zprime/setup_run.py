import os
import sys

sample_lists = {
    "005": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988350.MGPy8EG_30nlo_Leptophilicmutau_2tauZp005_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "007": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988351.MGPy8EG_30nlo_Leptophilicmutau_2tauZp007_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "009": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988352.MGPy8EG_30nlo_Leptophilicmutau_2tauZp009_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "010": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988395.MGPy8EG_30nlo_Leptophilicmutau_2tauZp010_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "011": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988353.MGPy8EG_30nlo_Leptophilicmutau_2tauZp011_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "013": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988354.MGPy8EG_30nlo_Leptophilicmutau_2tauZp013_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "015": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988355.MGPy8EG_30nlo_Leptophilicmutau_2tauZp015_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "017": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988356.MGPy8EG_30nlo_Leptophilicmutau_2tauZp017_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "019": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988357.MGPy8EG_30nlo_Leptophilicmutau_2tauZp019_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "023": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988358.MGPy8EG_30nlo_Leptophilicmutau_2tauZp023_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "027": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988359.MGPy8EG_30nlo_Leptophilicmutau_2tauZp027_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "031": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988360.MGPy8EG_30nlo_Leptophilicmutau_2tauZp031_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "035": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988361.MGPy8EG_30nlo_Leptophilicmutau_2tauZp035_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "039": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988362.MGPy8EG_30nlo_Leptophilicmutau_2tauZp039_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "042": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988363.MGPy8EG_30nlo_Leptophilicmutau_2tauZp042_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "045": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988364.MGPy8EG_30nlo_Leptophilicmutau_2tauZp045_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "048": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988365.MGPy8EG_30nlo_Leptophilicmutau_2tauZp048_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "051": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988366.MGPy8EG_30nlo_Leptophilicmutau_2tauZp051_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "054": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988367.MGPy8EG_30nlo_Leptophilicmutau_2tauZp054_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "057": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988368.MGPy8EG_30nlo_Leptophilicmutau_2tauZp057_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "060": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988369.MGPy8EG_30nlo_Leptophilicmutau_2tauZp060_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "063": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988370.MGPy8EG_30nlo_Leptophilicmutau_2tauZp063_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "066": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988371.MGPy8EG_30nlo_Leptophilicmutau_2tauZp066_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "069": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988372.MGPy8EG_30nlo_Leptophilicmutau_2tauZp069_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "072": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988373.MGPy8EG_30nlo_Leptophilicmutau_2tauZp072_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "075": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988374.MGPy8EG_30nlo_Leptophilicmutau_2tauZp075_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
}

def create_JO(mass, sample, sub_folder):
    job_name = "Zp{}.py".format(mass)
    file = open(sub_folder + "/" + job_name, "w+")
    template_cfg = """
import os, glob
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AlgSequence import AlgSequence
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = -1

svcMgr.EventSelector.InputCollections = glob.glob("{fsample}/*EVNT*")

job = AlgSequence()

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD'] + '/..'

rivet.Analyses += ["Z4mu"]
rivet.RunName = ''
rivet.HistoFile = 'Zp{fmass}_out_.yoda.gz'
#rivet.CrossSection =
#rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True
job += rivet

    """
    file.write(template_cfg.format(fmass=mass, fsample=sample))
    file.close()

if __name__ == "__main__":
    if len(sys.argv) > 1:
        run_folder = sys.argv[1]
    else:
        run_folder = "./"
    for mass in sample_lists:
        sub_folder = run_folder + "Zp" + mass
        if not os.path.exists(sub_folder):
            os.makedirs(sub_folder)
        sample = sample_lists[mass]
        create_JO(mass, sample, sub_folder)

    # create scale script
    """ doesn't work, use plot scale option instead
    with open("yoda_scale_from_1.sh", "w") as scale_from_1:
        for mass in sample_lists:
            sub_folder = run_folder + "Zp" + mass
            scale_from_1.write("cd {}\n".format(sub_folder))
            scale_from_1.write("yodamerge -o Zp{fmass}_out_normed.yoda.gz Zp{fmass}_out_.yoda.gz:{fscale}\n".format(fmass=mass, fscale=norm_dict[float(mass)]))
            scale_from_1.write("cd ..\n")
    """
