import os
import sys

sample_lists = {
    "069_2muZp": "/net/s3_datab/daits/g21.6.56/evgen/mc16.100022.MGPy8EG_30nlo_Leptophilicmutau_2muZp069_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "072_2muZp": "/net/s3_datab/daits/g21.6.56/evgen/mc16.100023.MGPy8EG_30nlo_Leptophilicmutau_2muZp072_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "075_2muZp": "/net/s3_datab/daits/g21.6.56/evgen/mc16.100024.MGPy8EG_30nlo_Leptophilicmutau_2muZp075_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",

    "069_2tauZp": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988372.MGPy8EG_30nlo_Leptophilicmutau_2tauZp069_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "072_2tauZp": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988373.MGPy8EG_30nlo_Leptophilicmutau_2tauZp072_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "075_2tauZp": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988374.MGPy8EG_30nlo_Leptophilicmutau_2tauZp075_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",

    "069_2mu2tau": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988222.MGPy8EG_30nlo_LHilicmutau_2muZp069taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "072_2mu2tau": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988223.MGPy8EG_30nlo_LHilicmutau_2muZp072taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
    "075_2mu2tau": "/net/s3_datab/daits/g21.6.56/evgen/mc16.988224.MGPy8EG_30nlo_LHilicmutau_2muZp075taus_4mu_4pt2.evgen.EVNT.e8304_pUM999999/",
}


def create_JO(mass, sample, sub_folder):
    job_name = "Zp{}.py".format(mass)
    file = open(sub_folder + "/" + job_name, "w+")
    template_cfg = """
import os, glob
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AlgSequence import AlgSequence
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = -1

svcMgr.EventSelector.InputCollections = glob.glob("{fsample}/*EVNT*")

job = AlgSequence()

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']

rivet.Analyses += ["Z4mu"]
rivet.RunName = ''
rivet.HistoFile = 'Zp{fmass}_out_.yoda.gz'
#rivet.CrossSection =
#rivet.IgnoreBeamCheck = True
rivet.SkipWeights=True
job += rivet

    """
    file.write(template_cfg.format(fmass=mass, fsample=sample))
    file.close()


if __name__ == "__main__":
    if len(sys.argv) > 1:
        run_folder = sys.argv[1]
    else:
        run_folder = "./"

    for mass in sample_lists:
        sub_folder = run_folder + "Zp" + mass
        if not os.path.exists(sub_folder):
            os.makedirs(sub_folder)
        sample = sample_lists[mass]
        create_JO(mass, sample, sub_folder)

        template = ""
        with open("Z4mu_Analysis_template.cc") as file:
            template = file.read()
        m = int(mass[:3])
        sigma_range = 5.0 * (-0.0202966 + 0.0190822 * m)
        low_limit = m - sigma_range
        up_limit = m + sigma_range
        template = template.replace("__LOW_LIMIT__", str(low_limit))
        template = template.replace("__UP_LIMIT__", str(up_limit))

        with open(sub_folder + "/Z4mu_Analysis.cc", "w+") as code:
            code.write(template)
            code.close()


