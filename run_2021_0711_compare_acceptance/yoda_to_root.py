import csv
import sys
from array import array

import ROOT as rt
import yoda

if len(sys.argv) < 2:
    print "Usage: python yoda_to_root.py <path to yoda> (path could be regex)"
    exit(-1)

accept_list = list()
for yoda_path in sorted(sys.argv[1:]):
    print "Converting:", yoda_path

    yodaAOs = yoda.read(yoda_path)
    root_path = yoda_path.replace(".yoda.gz", ".root")
    rtFile = rt.TFile(root_path, "recreate")
    for name in yodaAOs:
        yodaAO = yodaAOs[name]
        rtAO = None
        if "Histo1D" in str(yodaAO):
            rtAO = rt.TH1D(name, "", yodaAO.numBins(), array("d", yodaAO.xEdges()))
            rtAO.Sumw2()
            rtErrs = rtAO.GetSumw2()
            for i in range(rtAO.GetNbinsX()):
                rtAO.SetBinContent(i + 1, yodaAO.bin(i).sumW())
                rtErrs.AddAt(yodaAO.bin(i).sumW2(), i + 1)
            if (
                "passed" in str(yodaAO)
                and "RAW" not in str(yodaAO)
                and "Zp010" not in str(yoda_path)
            ):
                evnt_cut = yodaAO.bin(0).sumW()
                evnt_pass = yodaAO.bin(1).sumW()
                accept = evnt_pass / (evnt_pass + evnt_cut)
                accept_list.append(accept)
                print "accept = ", accept
        elif "Scatter2D" in str(yodaAO):
            rtAO = rt.TGraphAsymmErrors(yodaAO.numPoints())
            for i in range(yodaAO.numPoints()):
                x = yodaAO.point(i).x()
                y = yodaAO.point(i).y()
                xLo, xHi = yodaAO.point(i).xErrs()
                yLo, yHi = yodaAO.point(i).yErrs()
                rtAO.SetPoint(i, x, y)
                rtAO.SetPointError(i, xLo, xHi, yLo, yHi)
        else:
            continue
        rtAO.Write(name)
    rtFile.Close()

print (accept_list[:13])
print (accept_list[13:])

with open('low_mass.txt', 'w') as f:
      
    # using csv.writer method from CSV package
    write = csv.writer(f)
      
    write.writerow(accept_list[:13])
