// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"

namespace Rivet {

/// ATLAS 4-lepton lineshape at 13 TeV
class Z4mu : public Analysis {
   public:
    Z4mu() : Analysis("Z4mu") {}

    void init() {
        PromptFinalState photons(Cuts::abspid == PID::PHOTON);
        PromptFinalState muons(Cuts::abspid == PID::MUON);
        Cut mu_fid_sel = (Cuts::abseta < 2.7) && (Cuts::pT > 3 * GeV);
        DressedLeptons dressed_muons(photons, muons, 0.005, mu_fid_sel, false);
        declare(dressed_muons, "muons");

        // Book histos
        book(m_hists["mu1_pt"], "mu1_pt", 40, 5, 200);
        book(m_hists["mu2_pt"], "mu2_pt", 40, 5, 100);
        book(m_hists["mu3_pt"], "mu3_pt", 40, 5, 100);
        book(m_hists["mu4_pt"], "mu4_pt", 40, 5, 100);
        book(m_hists["mu1_eta"], "mu1_eta", 40, -3.5, 3.5);
        book(m_hists["mu2_eta"], "mu2_eta", 40, -3.5, 3.5);
        book(m_hists["mu3_eta"], "mu3_eta", 40, -3.5, 3.5);
        book(m_hists["mu4_eta"], "mu4_eta", 40, -3.5, 3.5);
        book(m_hists["mu1_phi"], "mu1_phi", 40, 0, TWOPI);
        book(m_hists["mu2_phi"], "mu2_phi", 40, 0, TWOPI);
        book(m_hists["mu3_phi"], "mu3_phi", 40, 0, TWOPI);
        book(m_hists["mu4_phi"], "mu4_phi", 40, 0, TWOPI);

        book(m_hists["z1_pt"], "z1_pt", 40, 5, 200);
        book(m_hists["z2_pt"], "z2_pt", 40, 5, 100);
        book(m_hists["z1_eta"], "z1_eta", 40, -3.5, 3.5);
        book(m_hists["z2_eta"], "z2_eta", 40, -3.5, 3.5);
        book(m_hists["z1_phi"], "z1_phi", 40, 0, TWOPI);
        book(m_hists["z2_phi"], "z2_phi", 40, 0, TWOPI);
        book(m_hists["z1_m"], "z1_m", 40, 5, 200);
        book(m_hists["z2_m"], "z2_m", 40, 5, 200);
        book(m_hists["z1_dR"], "z1_dR", 25, 0.5, 7.0);
        book(m_hists["z2_dR"], "z2_dR", 25, 0.5, 7.0);
        book(m_hists["z1_deltaEta"], "z1_deltaEta", 40, -3.5, 3.5);
        book(m_hists["z2_deltaEta"], "z2_deltaEta", 40, -3.5, 3.5);

        book(m_hists["4mu_pt"], "4mu_pt", 40, 5, 200);
        book(m_hists["4mu_m"], "4mu_m", 40, 5, 200);
        book(m_hists["4mu_y"], "4mu_y", 40, 0, 5);

        book(m_hists["passed_events_count"], "passed_events_count", 2, 0, 2);
    }

    // Do the analysis
    void analyze(const Event& event) {
        // preselection of leptons for Z' -> 4mu final states
        Particles dressed_muons;
        for (auto lep : apply<DressedLeptons>(event, "muons").dressedLeptons()) {
            dressed_muons.push_back(lep);
        }
        auto foundDressed = getBestQuads(dressed_muons);
        if (foundDressed.empty()) {
            m_hists["passed_events_count"]->fill(0);
            vetoEvent;
        } else {
            m_hists["passed_events_count"]->fill(1);
        }
        if (passSelection(foundDressed[0])) {
            Quadruplet::FlavCombi flavour = foundDressed[0].type();
            if (flavour == Quadruplet::FlavCombi::mm) {
                m_hists["mu1_pt"]->fill(foundDressed[0].get_mu1().pT() / GeV);
                m_hists["mu2_pt"]->fill(foundDressed[0].get_mu2().pT() / GeV);
                m_hists["mu3_pt"]->fill(foundDressed[0].get_mu3().pT() / GeV);
                m_hists["mu4_pt"]->fill(foundDressed[0].get_mu4().pT() / GeV);
                m_hists["mu1_eta"]->fill(foundDressed[0].get_mu1().eta());
                m_hists["mu2_eta"]->fill(foundDressed[0].get_mu2().eta());
                m_hists["mu3_eta"]->fill(foundDressed[0].get_mu3().eta());
                m_hists["mu4_eta"]->fill(foundDressed[0].get_mu4().eta());
                m_hists["mu1_phi"]->fill(foundDressed[0].get_mu1().phi());
                m_hists["mu2_phi"]->fill(foundDressed[0].get_mu2().phi());
                m_hists["mu3_phi"]->fill(foundDressed[0].get_mu3().phi());
                m_hists["mu4_phi"]->fill(foundDressed[0].get_mu4().phi());

                m_hists["z1_pt"]->fill(foundDressed[0].getZ1().mom().pT() / GeV);
                m_hists["z2_pt"]->fill(foundDressed[0].getZ2().mom().pT() / GeV);
                m_hists["z1_eta"]->fill(foundDressed[0].getZ1().mom().eta());
                m_hists["z2_eta"]->fill(foundDressed[0].getZ2().mom().eta());
                m_hists["z1_phi"]->fill(foundDressed[0].getZ1().mom().phi());
                m_hists["z2_phi"]->fill(foundDressed[0].getZ2().mom().phi());
                m_hists["z1_m"]->fill(foundDressed[0].getZ1().mom().mass() / GeV);
                m_hists["z2_m"]->fill(foundDressed[0].getZ2().mom().mass() / GeV);
                m_hists["z1_dR"]->fill(foundDressed[0].getZ1().dR());
                m_hists["z2_dR"]->fill(foundDressed[0].getZ2().dR());
                m_hists["z1_deltaEta"]->fill(foundDressed[0].getZ1().delta_mom().eta());
                m_hists["z2_deltaEta"]->fill(foundDressed[0].getZ2().delta_mom().eta());

                m_hists["4mu_pt"]->fill(foundDressed[0].mom().pT() / GeV);
                m_hists["4mu_m"]->fill(foundDressed[0].mom().mass() / GeV);
                m_hists["4mu_y"]->fill(foundDressed[0].mom().absrap());
            }
        }
    }

    /// Finalize
    void finalize() { normalize(m_hists); }

    struct Dilepton : public ParticlePair {
        Dilepton() {}
        Dilepton(ParticlePair _particlepair) : ParticlePair(_particlepair) {
            assert(first.abspid() == second.abspid());
        }
        FourMomentum mom() const { return first.momentum() + second.momentum(); }
        FourMomentum delta_mom() const { return first.momentum() - second.momentum(); }
        operator FourMomentum() const { return mom(); }
        static bool cmppT(const Dilepton& lx, const Dilepton& rx) { return lx.mom().pT() < rx.mom().pT(); }
        int flavour() const { return first.abspid(); }
        double pTl1() const { return first.pT(); }
        double pTl2() const { return second.pT(); }
        double dR() const { return deltaR(first.momentum(), second.momentum()); }
    };

    struct Quadruplet {
        Quadruplet(Dilepton z1, Dilepton z2) {
            if (z1.mom().mass() > z2.mom().mass()) {
                _z1 = z1;
                _z2 = z2;
            } else {
                _z1 = z2;
                _z2 = z1;
            }
            Particles all_mus{z1.first, z1.second, z2.first, z2.second};
            std::sort(all_mus.begin(), all_mus.end(),
                      [](const Particle& p1, const Particle& p2) { return p1.pT() > p2.pT(); });
            mu_1 = all_mus[0];
            mu_2 = all_mus[1];
            mu_3 = all_mus[2];
            mu_4 = all_mus[3];
        }
        enum class FlavCombi { mm = 0, ee, me, em, undefined };
        FourMomentum mom() const { return _z1.mom() + _z2.mom(); }
        Dilepton getZ1() const { return _z1; }
        Dilepton getZ2() const { return _z2; }
        Particle get_mu1() const { return mu_1; }
        Particle get_mu2() const { return mu_2; }
        Particle get_mu3() const { return mu_3; }
        Particle get_mu4() const { return mu_4; }
        Dilepton _z1, _z2;
        Particle mu_1;
        Particle mu_2;
        Particle mu_3;
        Particle mu_4;
        FlavCombi type() const {
            if (_z1.flavour() == 13 && _z2.flavour() == 13) {
                return FlavCombi::mm;
            } else if (_z1.flavour() == 11 && _z2.flavour() == 11) {
                return FlavCombi::ee;
            } else if (_z1.flavour() == 13 && _z2.flavour() == 11) {
                return FlavCombi::me;
            } else if (_z1.flavour() == 11 && _z2.flavour() == 13) {
                return FlavCombi::em;
            } else
                return FlavCombi::undefined;
        }
    };

    bool haveSameLeptons(const Dilepton& p1, const Dilepton& p2) {
        double pT_1_1 = p1.pTl1();
        double pT_1_2 = p1.pTl2();
        double pT_2_1 = p2.pTl1();
        double pT_2_2 = p2.pTl2();
        if (pT_1_1 == pT_2_1 || pT_1_1 == pT_2_2 || pT_1_2 == pT_2_1 || pT_1_2 == pT_2_2) {
            return true;
        } else
            return false;
    }

    vector<Quadruplet> getBestQuads(Particles& particles) {
        // H->ZZ->4l pairing
        // - Two same flavor opposite charged leptons
        // - Ambiguities in pairing are resolved by choosing the combination
        //     that results in the smaller value of |mll - mZ| for each pair successively
        vector<Quadruplet> quads{};

        size_t n_parts = particles.size();
        if (n_parts < 4) return quads;

        // STEP 1: find SFOS pairs
        vector<Dilepton> SFOS;
        for (size_t i = 0; i < n_parts; ++i) {
            for (size_t j = 0; j < i; ++j) {
                if (particles[i].pid() == -particles[j].pid()) {
                    // sort such that the negative lepton is listed first
                    if (particles[i].pid() > 0)
                        SFOS.push_back(Dilepton(make_pair(particles[i], particles[j])));
                    else
                        SFOS.push_back(Dilepton(make_pair(particles[j], particles[i])));
                }
            }
        }
        if (SFOS.size() < 2) return quads;

        // now we sort the SFOS pairs
        std::sort(SFOS.begin(), SFOS.end(), [](const Dilepton& p1, const Dilepton& p2) {
            return fabs(p1.mom().mass() - Z_mass) < fabs(p2.mom().mass() - Z_mass);
        });

        // Form all possible quadruplets passing the pT cuts
        for (size_t k = 0; k < SFOS.size(); ++k) {
            for (size_t l = k + 1; l < SFOS.size(); ++l) {
                if (deltaR(SFOS[k].first.mom(), SFOS[l].first.mom()) < 1e-13) continue;
                if (deltaR(SFOS[k].first.mom(), SFOS[l].second.mom()) < 1e-13) continue;
                if (deltaR(SFOS[k].second.mom(), SFOS[l].first.mom()) < 1e-13) continue;
                if (deltaR(SFOS[k].second.mom(), SFOS[l].second.mom()) < 1e-13) continue;

                vector<double> lep_pt{SFOS[k].pTl1(), SFOS[k].pTl2(), SFOS[l].pTl1(), SFOS[l].pTl2()};
                std::sort(lep_pt.begin(), lep_pt.end(), std::greater<double>());
                if (!(lep_pt[0] > 20 * GeV && lep_pt[1] > 15 * GeV && lep_pt[2] > 8 * GeV)) continue;
                if (haveSameLeptons(SFOS[k], SFOS[l])) continue;
                quads.push_back(Quadruplet(SFOS[k], SFOS[l]));
            }
        }
        return quads;
    }

    bool pass_dRll(const Quadruplet& theQuad) {
        const double dR_min_same = 0.1;
        const double dR_min_opp = 0.2;
        double dr_min_cross = dR_min_opp;
        if (theQuad.getZ1().flavour() == theQuad.getZ2().flavour()) {
            dr_min_cross = dR_min_same;
        }
        return !((deltaR(theQuad.getZ1().first, theQuad.getZ1().second) < dR_min_same) ||
                 (deltaR(theQuad.getZ2().first, theQuad.getZ2().second) < dR_min_same) ||
                 (deltaR(theQuad.getZ1().first, theQuad.getZ2().first) < dr_min_cross) ||
                 (deltaR(theQuad.getZ1().first, theQuad.getZ2().second) < dr_min_cross) ||
                 (deltaR(theQuad.getZ1().second, theQuad.getZ2().first) < dr_min_cross) ||
                 (deltaR(theQuad.getZ1().second, theQuad.getZ2().second) < dr_min_cross));
    }

    // Handle 3 further CF stages - m12/34, dRmin, jpsi veto
    bool passSelection(const Quadruplet& theQuad) { return pass_dRll(theQuad); }

   private:
    map<string, Histo1DPtr> m_hists;
    static constexpr double Z_mass = 91.1876;
};

DECLARE_RIVET_PLUGIN(Z4mu);

}  // namespace Rivet