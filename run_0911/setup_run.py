import os
import sys

sample_lists = {
    "005": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988600.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp005_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "007": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988601.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp007_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "009": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988602.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp009_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "010": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988690.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp010_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "011": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988603.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp011_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "013": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988604.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp013_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "015": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988605.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp015_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "017": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988606.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp017_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "019": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988607.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp019_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "023": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988608.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp023_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "027": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988609.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp027_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "031": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988610.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp031_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "035": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988611.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp035_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "039": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988612.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp039_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "042": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988613.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp042_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "045": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988614.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp045_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "048": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988615.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp048_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "051": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988616.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp051_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "054": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988617.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp054_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "057": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988618.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp057_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "060": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988619.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp060_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "063": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988620.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp063_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "066": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988621.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp066_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "069": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988622.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp069_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "072": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988623.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp072_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
    "075": "/net/s3_datab/daits/p19.2.5.33.3/evgen/mc15.988624.MGPy8EG_NNPDF30nlo_Leptophilicmutau_2muZp075_4mu_4pt2.evgen.EVNT.e7056_pUM999999",
}


def create_JO(mass, sample, sub_folder):
    job_name = "Zp{}.py".format(mass)
    file = open(sub_folder + "/" + job_name, "w+")
    template_cfg = """
import os, glob
from Rivet_i.Rivet_iConf import Rivet_i
from AthenaCommon.AlgSequence import AlgSequence
import AthenaPoolCnvSvc.ReadAthenaPool
theApp.EvtMax = -1

svcMgr.EventSelector.InputCollections = glob.glob("{fsample}/*EVNT*")

job = AlgSequence()

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD'] + '/..'

rivet.Analyses += ["Z4mu"]
rivet.RunName = ''
rivet.HistoFile = 'Zp{fmass}_out_.yoda.gz'
rivet.CrossSection = 1.0
#rivet.IgnoreBeamCheck = True
# rivet.SkipWeights=True
job += rivet

    """
    file.write(template_cfg.format(fmass=mass, fsample=sample))
    file.close()


if __name__ == "__main__":
    if len(sys.argv) > 1:
        run_folder = sys.argv[1]
    else:
        run_folder = "./"
    for mass in sample_lists:
        sub_folder = run_folder + "Zp" + mass
        if not os.path.exists(sub_folder):
            os.makedirs(sub_folder)
        sample = sample_lists[mass]
        create_JO(mass, sample, sub_folder)

